(ns mrdt.mergeable)

(defprotocol Mergeable
  (abstractify [this])
  (three-way-merge [r1 r2 lca]))

(defprotocol Identifiable
  (val-ids [this])
  (val-map [this]))
