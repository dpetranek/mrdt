(ns mrdt.mergeable-seq
  (:require [clojure.set :as set]
            [clojure.pprint :as pprint]
            [mrdt.graph :as graph]
            [mrdt.mergeable :as m])
  (:import (java.util Collection)))

(defn build-ob
  "Build the observed-before (ob) relation relating every pair of elements x, y in coll such
  that x occurs before y."
  [coll]
  (loop [[head & rest] coll
         ob []]
    (if head
      (recur rest (into ob (for [x rest] [head x])))
      ob)))

(defn build-kv
  "Build the key-value relation for a map."
  [m]
  (set m))

(defn seq->relations
  "Abstractify a mergeable sequence into its characteristic relations: membership (:mem),
  order (:ord), and a map of value id->value."
  [ms]
  (let [val-ids (m/val-ids ms)]
    {:mem (set val-ids)
     :ord (build-ob val-ids)
     :val-map (m/val-map ms)}))

(defn relations->seq
  "Given the characteristic relations of a sequence, return a mergeable seq."
  [{:keys [mem ord val-map]}]
  (cond
    (empty? mem) []
    (empty? ord) (seq mem)
    :else
    (->> ord
         (graph/build-graph)
         (graph/topo-sort)
         (map #(vector % (val-map %))))))

(defn product
  [s1 s2]
  (set (for [a s1 b s2]
         [a b])))

(defn three-way-merge-sequence
  [r1 r2 lca]
  (let [{lca-mem :mem lca-ord :ord :as lca-a} (m/abstractify lca)
        {r1-mem :mem r1-ord :ord :as r1-a} (m/abstractify r1)
        {r2-mem :mem r2-ord :ord :as r2-a} (m/abstractify r2)
        merged-mem (set/union (set/intersection lca-mem r1-mem r2-mem)
                              (set/difference r1-mem lca-mem)
                              (set/difference r2-mem lca-mem))]
    (relations->seq {:mem merged-mem
                     :ord (set/intersection (set/union (set/intersection lca-ord r1-ord r2-ord)
                                                       (set/difference r1-ord lca-ord)
                                                       (set/difference r2-ord lca-ord))
                                            ;; the new order can only contain merged members
                                            (product merged-mem merged-mem))
                     :val-map (merge (:val-map lca-a) (:val-map r1-a) (:val-map r2-a))})))

(deftype MergeableSeq [meta xs size]
  clojure.lang.Counted
  (count [this] size)

  clojure.lang.IPersistentCollection
  (cons [this x]
    ;; we should probably be more efficient with our base structure
    (MergeableSeq. meta (conj xs [(java.util.UUID/randomUUID) x]) (inc size)))
  (empty [this]
    (with-meta (MergeableSeq. {:mergeable true} [] 0) meta))
  (equiv [this x] (.equals xs x))

  clojure.lang.Seqable
  (seq [this] (seq xs))

  clojure.lang.IMeta
  (meta [this] meta)

  clojure.lang.IObj
  (withMeta [this m]
    (MergeableSeq. m xs size))

  Object
  (equals [this x]
    (if (instance? MergeableSeq x)
      (.equals xs) (.xs x)))
  (hashCode [this]
    (hash-combine (hash xs) MergeableSeq))
  (toString [this] (str "#mergeable " `[~@(map second xs)]))

  java.util.Collection
  (isEmpty [this]
    (zero? size))
  (size [this] size)
  (^objects toArray [this ^objects a]
   (.toArray ^Collection (or (seq this) ()) a))
  (toArray [this]
    (.toArray ^Collection (or (seq this) ())))
  (iterator [this]
    (.iterator ^Collection (or (seq this) ())))
  (containsAll [this coll]
    (.containsAll ^Collection (into #{} this) coll))

  ;; consider storing this stuff in metadata instead of as another protocol
  m/Identifiable
  (m/val-ids [this] (map first xs))
  (m/val-map [this] (into {} xs))

  m/Mergeable
  (m/abstractify [s] (seq->relations s))
  (m/three-way-merge [r1 r2 lca]
    (let [raw (three-way-merge-sequence r1 r2 lca)]
      (MergeableSeq. {:mergeable true} raw (count raw)))))

(defn mergeable?
  [x]
  (instance? MergeableSeq x))

(defn mergeable-seq
  ([]
   (->MergeableSeq {:mergeable true} [] 0))
  ([xs]
   (->MergeableSeq {:mergeable true} (mapv #(vector (java.util.UUID/randomUUID) %) xs) (count xs))))

(defmethod print-method MergeableSeq
  [^MergeableSeq mergeable ^java.io.Writer w]
  (.write w (.toString mergeable)))

(defmethod pprint/simple-dispatch MergeableSeq
  [^MergeableSeq mergeable]
  (pr mergeable))

(defn mrdt-reader
  [xs]
  (mergeable-seq xs))

(comment
  (def a #mergeable [1])
  #mergeable [1]

  (def r1 (conj a 3))
  (def r2 (conj a 2))
  r1
  #mergeable [1 3]
  r2
  #mergeable [1 2]
  (m/three-way-merge r1 r2 a )
  #mergeable [1 2 3]

  (m/abstractify a)

  (seq->relations a)
  {:mem (#uuid "9ca02024-0dcb-495b-abd8-9c5dc1c9c86e"), :ord [], :val-map {#uuid "9ca02024-0dcb-495b-abd8-9c5dc1c9c86e" 1}}

  (three-way-merge-sequence r2 r1 a)

  '(2 1 3)
  (m/three-way-merge r2 r1 a )
  '(2 1 3)

  (def b #mergeable [1 2 1])
  (def r3 (filter odd? (conj a 3)))
  (def r4 (reverse (conj a 2)))

  (m/three-way-merge r3 r4 b)


  (val-ids (map inc #mergeable [1 2 3]))




  (mergeable-seq)
  #mergeable []


  (val-ids a)
  ([#uuid "d7429b52-159c-40e4-af9f-ff0b1ed1befe" 1] [#uuid "507557ac-bd57-4437-aef1-3d29390e6d16" 2] [#uuid "4f0ccbad-69a9-48f0-9be0-d650f713a4bb" 3])
  (val-ids (conj a 1))
  (into {} '([#uuid "f12fda3b-aa92-46ba-ac0c-4dd5c1779063" 1] [#uuid "d7429b52-159c-40e4-af9f-ff0b1ed1befe" 1] [#uuid "507557ac-bd57-4437-aef1-3d29390e6d16" 2] [#uuid "4f0ccbad-69a9-48f0-9be0-d650f713a4bb" 3]))



  #mergeable [1 1 2 3])
